package com.Exerc.cio.integradorspringboot.controleacademico.services;

import com.Exerc.cio.integradorspringboot.controleacademico.exceptions.DisciplinaNaoEncontradaException;
import com.Exerc.cio.integradorspringboot.controleacademico.exceptions.EstudanteNaoEncontradoException;
import com.Exerc.cio.integradorspringboot.controleacademico.exceptions.IdInvalidoException;
import com.Exerc.cio.integradorspringboot.controleacademico.exceptions.MatriculaJaRealizadaException;
import com.Exerc.cio.integradorspringboot.controleacademico.models.Disciplina;
import com.Exerc.cio.integradorspringboot.controleacademico.models.Endereco;
import com.Exerc.cio.integradorspringboot.controleacademico.models.Estudante;
import com.Exerc.cio.integradorspringboot.controleacademico.repositories.DisciplinaRepository;
import com.Exerc.cio.integradorspringboot.controleacademico.repositories.EstudanteRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.beans.PropertyDescriptor;
import java.util.*;

@Service
@RequiredArgsConstructor
public class EstudanteService {

    private final EstudanteRepository estudanteRepository;

    private  final DisciplinaRepository disciplinaRepository;

    public Estudante adiciona(Estudante estudante){

        if(estudanteRepository.existsById(estudante.getNumDocumento()))
            throw  new IdInvalidoException("Numero de documento ja cadastrado!");

        return estudanteRepository.save(estudante);
    }

    public List<Estudante> buscaEstudantesPorNome(String termo){

        if(estudanteRepository.busca("%"+termo+"%").isEmpty())
            throw new EstudanteNaoEncontradoException("Não há alunos com esse nome!");

        return estudanteRepository.busca("%"+termo+"%");

    }

    public Page<Estudante> listarTodos(Pageable pageable){
        return estudanteRepository.findAll(pageable);
    }

    public  Estudante buscaPelaMatricula(String nMatricula){

        return  estudanteRepository.getByMatricula(nMatricula)
                .orElseThrow(() -> new EstudanteNaoEncontradoException("Matricula não existe!"));
    }

    public Estudante buscaEstudantePeloIdOuLancaException(String id){

        return  estudanteRepository.findById(id)
                .orElseThrow(()-> new EstudanteNaoEncontradoException("Estudante não encontrado"));
    }
    public Estudante atualizar(String numDocumento, Estudante estudante) {

        Estudante estudanteAtualizado = buscaEstudantePeloIdOuLancaException(numDocumento);

        BeanUtils.copyProperties(estudante,estudanteAtualizado,retornaNomeDeAtributosNulos(estudante));

        return estudanteRepository.save(estudanteAtualizado);
    }

    public Estudante adicionaEndereco(String idEstudante,Endereco endereco){

        Estudante estudante= buscaEstudantePeloIdOuLancaException(idEstudante);

        estudante.setEndereco(endereco);

        return  estudanteRepository.save(estudante);
    }

    public Estudante adicionarDisciplina(String numMatricula,String codigoDisciplina, Integer turma){

       Estudante estudante= estudanteRepository.getByMatricula(numMatricula)
               .orElseThrow(()->new EstudanteNaoEncontradoException("Estudante nao encontrado!"));


        Disciplina disciplina = disciplinaRepository.getByCodigoAndTurma(codigoDisciplina,turma).
                orElseThrow(()->new DisciplinaNaoEncontradaException("Disciplina nao encontrada!"));


        if(estudante.getDisciplinas()==null || estudante.getDisciplinas().isEmpty()){
            estudante.setDisciplinas(new ArrayList<>());
        }

        if(estudante.getDisciplinas().contains(disciplina)){
            throw new MatriculaJaRealizadaException("Estudante já está matriculado na disciplina!");
        }

        estudante.getDisciplinas().add(disciplina);

        return estudanteRepository.save(estudante);
    }

    public List<Disciplina> listarDisciplinas(String idEstudante){

        Estudante estudante= buscaEstudantePeloIdOuLancaException(idEstudante);

        if( estudante.getDisciplinas()==null)
            throw new DisciplinaNaoEncontradaException("Estudante nao possui nenhuma disciplina!");

        return  estudante.getDisciplinas();
    }

    public static String[] retornaNomeDeAtributosNulos(Estudante estudante) {
        final BeanWrapper src = new BeanWrapperImpl(estudante);
        PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> nomesVazios = new HashSet<String>();
        for (PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null || srcValue=="")
                nomesVazios.add(pd.getName());
        }
        nomesVazios.add("numDocumento");
        String[] resultado = new String[nomesVazios.size()];
        return nomesVazios.toArray(resultado);
    }
}
