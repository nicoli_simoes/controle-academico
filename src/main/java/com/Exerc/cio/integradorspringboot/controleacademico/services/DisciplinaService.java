package com.Exerc.cio.integradorspringboot.controleacademico.services;

import com.Exerc.cio.integradorspringboot.controleacademico.exceptions.DisciplinaJaCadastrada;
import com.Exerc.cio.integradorspringboot.controleacademico.exceptions.DisciplinaNaoEncontradaException;
import com.Exerc.cio.integradorspringboot.controleacademico.exceptions.EstudanteNaoEncontradoException;
import com.Exerc.cio.integradorspringboot.controleacademico.models.Disciplina;
import com.Exerc.cio.integradorspringboot.controleacademico.models.Estudante;
import com.Exerc.cio.integradorspringboot.controleacademico.repositories.DisciplinaRepository;
import com.Exerc.cio.integradorspringboot.controleacademico.repositories.EstudanteRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.stereotype.Service;

import java.beans.PropertyDescriptor;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class DisciplinaService {
    private final DisciplinaRepository disciplinaRepository;

    private final EstudanteRepository estudanteRepository;

    public List<Disciplina> listarTodas(){
        return disciplinaRepository.findAll();
    }

    public Disciplina addDisciplina(Disciplina disciplina){

        if(disciplinaRepository.getByCodigoAndNomeAndTurma(disciplina.getCodigo(),
                disciplina.getNome(),
                disciplina.getTurma()).
                isPresent()){
            throw  new DisciplinaJaCadastrada("Disciplina já foi cadastrada!");
        }

        return disciplinaRepository.save(disciplina);
    }

    public void deletarDisciplina(Long id){

        Disciplina disciplina = buscaDisciplinaPeloIdOuLancaException(id);

        disciplinaRepository.delete(disciplina);
    }

    public Disciplina atualizarDisciplina(Disciplina disciplina,Long id){

        Disciplina disciplinaAtualizada = buscaDisciplinaPeloIdOuLancaException(id);

        BeanUtils.copyProperties(disciplina,disciplinaAtualizada,retornaNomeDeAtributosNulos(disciplina));

        return disciplinaRepository.save(disciplinaAtualizada);
    }

    public Disciplina addEstudante (Long idDisciplina, String idEstudante){

        Estudante estudanteMatriculado = buscaEstudantePeloIdOuLancaException(idEstudante);

        Disciplina disciplinaAdd= buscaDisciplinaPeloIdOuLancaException(idDisciplina);

        disciplinaAdd.getEstudantes().add(estudanteMatriculado);

        return disciplinaRepository.save(disciplinaAdd);
    }

    public Disciplina buscaDisciplinaPeloIdOuLancaException(Long id){

        return disciplinaRepository.findById(id)
                .orElseThrow(()->new DisciplinaNaoEncontradaException("Disciplina não encontrada"));
    }

    public Estudante buscaEstudantePeloIdOuLancaException(String id){

        return  estudanteRepository.findById(id)
                .orElseThrow(()-> new EstudanteNaoEncontradoException("Estudante não encontrado"));
    }

    public static String[] retornaNomeDeAtributosNulos(Disciplina disciplina) {
        final BeanWrapper src = new BeanWrapperImpl(disciplina);
        PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> nomesVazios = new HashSet<String>();
        for (PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null || srcValue=="")
                nomesVazios.add(pd.getName());
        }
        nomesVazios.add("id");
        String[] resultado = new String[nomesVazios.size()];
        return nomesVazios.toArray(resultado);
    }
}
