package com.Exerc.cio.integradorspringboot.controleacademico.listeners;

import com.Exerc.cio.integradorspringboot.controleacademico.models.Estudante;

import javax.persistence.PrePersist;
import java.util.UUID;

public class EstudanteListener {

    @PrePersist
    public void antesDeCriar(Estudante estudante) {

        if(estudante.getMatricula()==null)
            estudante.setMatricula(geraNumeroMatricula());
    }

    String geraNumeroMatricula(){
        return UUID.randomUUID().toString();
    }
}
