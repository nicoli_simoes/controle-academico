package com.Exerc.cio.integradorspringboot.controleacademico.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class DisciplinaNaoEncontradaException extends  RuntimeException{

    public DisciplinaNaoEncontradaException(String mensagem){
        super(mensagem);
    }

}
