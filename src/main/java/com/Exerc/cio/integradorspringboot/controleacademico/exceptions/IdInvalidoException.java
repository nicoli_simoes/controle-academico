package com.Exerc.cio.integradorspringboot.controleacademico.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IdInvalidoException extends  RuntimeException{

    public IdInvalidoException(String mensagem){
        super(mensagem);
    }

}
