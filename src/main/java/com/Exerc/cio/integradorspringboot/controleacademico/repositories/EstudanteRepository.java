package com.Exerc.cio.integradorspringboot.controleacademico.repositories;

import com.Exerc.cio.integradorspringboot.controleacademico.models.Disciplina;
import com.Exerc.cio.integradorspringboot.controleacademico.models.Estudante;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EstudanteRepository extends JpaRepository<Estudante,String> {

    Optional<Estudante> getByMatricula(String matricula);

    @Query(value =  "select u from Estudante u where nome like :termo")
    List<Estudante> busca(@Param("termo") String termo);
}
