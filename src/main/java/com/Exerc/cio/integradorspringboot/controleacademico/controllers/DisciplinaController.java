package com.Exerc.cio.integradorspringboot.controleacademico.controllers;

import com.Exerc.cio.integradorspringboot.controleacademico.models.Disciplina;
import com.Exerc.cio.integradorspringboot.controleacademico.services.DisciplinaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/disciplinas")
public class DisciplinaController {
    @Autowired
    private DisciplinaService disciplinaService;

    @GetMapping
    public ResponseEntity<List<Disciplina>> listarTodas(){return ResponseEntity.ok(disciplinaService.listarTodas());}

    @PostMapping("/adicionar-disciplina")
    public ResponseEntity<Disciplina> addDisciplina(@RequestBody @Valid Disciplina disciplina){
        return ResponseEntity.status(HttpStatus.CREATED).body(disciplinaService.addDisciplina(disciplina));
    }

    @PutMapping("/atualizar-disciplina/{id}")
    public ResponseEntity<Disciplina> atualizarDisciplina(@PathVariable(value = "id")Long idDisciplina,@RequestBody Disciplina disciplina){
        return ResponseEntity.ok(disciplinaService.atualizarDisciplina(disciplina,idDisciplina));
    }

    @DeleteMapping("/deletar-disciplina/{id}")
    public ResponseEntity<Disciplina> deletarDisciplina(@PathVariable(value = "id")Long idDisciplina){
        disciplinaService.deletarDisciplina(idDisciplina);
        return  ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}
