package com.Exerc.cio.integradorspringboot.controleacademico.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Endereco {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotBlank(message = "Rua é obrigatório")
    @Column(name = "rua")
    private String rua;

    @NotBlank(message = "Cidade é obrigatório")
    @Column(name = "cidade")
    private String cidade;

    @NotBlank(message = "Estado é obrigatório")
    @Column(name = "estado")
    private String estado;

    @NotBlank(message = "CEP é obrigatório")
    @Column(name = "cep")
    private String cep;

    @OneToOne(mappedBy = "endereco")
    @JsonIgnoreProperties("endereco")
    private Estudante estudante;

}
