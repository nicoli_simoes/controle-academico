package com.Exerc.cio.integradorspringboot.controleacademico.controllers;

import com.Exerc.cio.integradorspringboot.controleacademico.models.Disciplina;
import com.Exerc.cio.integradorspringboot.controleacademico.models.Endereco;
import com.Exerc.cio.integradorspringboot.controleacademico.models.Estudante;
import com.Exerc.cio.integradorspringboot.controleacademico.services.EstudanteService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/estudantes")
@RequiredArgsConstructor
public class EstudanteController {

    private final EstudanteService estudanteService;

    @GetMapping
    public ResponseEntity<Page<Estudante>> listarTodos(@PageableDefault(page=0, size = 5, direction = Sort.Direction.ASC)
                                                           Pageable pageable){
        return ResponseEntity.ok(estudanteService.listarTodos(pageable));
    }

    @PostMapping("/adiciona")
    public  ResponseEntity<Estudante> add(@RequestBody @Valid Estudante estudante){
        return ResponseEntity.status(HttpStatus.CREATED).body(estudanteService.adiciona(estudante));
    }

    @PostMapping("/buscaPorNome")
    public  ResponseEntity<List<Estudante>> buscaPorNome(@RequestParam(value = "termo") String termo){
        return ResponseEntity.ok(estudanteService.buscaEstudantesPorNome(termo));
    }

    @GetMapping("/buscaPorMatricula/{nMatricula}")
    public  ResponseEntity<Estudante> buscaPorMatricula(@PathVariable(value = "nMatricula") String nMatricula){
        return ResponseEntity.ok(estudanteService.buscaPelaMatricula(nMatricula));
    }

    @PutMapping("/atualizar/{numDocumento}")
    public ResponseEntity<Estudante> atualizarEstudante(@PathVariable(value = "numDocumento")String numDocumento,
                                                         @RequestBody Estudante estudante){
        return ResponseEntity.ok(estudanteService.atualizar(numDocumento,estudante));
    }

    @PostMapping("/{idEstudante}/adicionar/endereco")
    private  ResponseEntity<Estudante> adicionaEndereco(@RequestBody @Valid Endereco endereco,
                                                        @PathVariable(value = "idEstudante") String id){

       return ResponseEntity.ok(estudanteService.adicionaEndereco(id,endereco));

    }

    @PostMapping("/matricular")
    private  ResponseEntity<Estudante> adicionarDisciplina(@RequestParam(value = "matricula") String matricula,
                                                           @RequestParam(value = "turma") Integer turma,
                                                           @RequestParam(value="codigo") String codigo){

        return ResponseEntity.ok(estudanteService.adicionarDisciplina(matricula,codigo,turma));
    }

    @GetMapping("/{idEstudante}/disciplinas")
    private ResponseEntity<List<Disciplina>> listarDisciplinas(@PathVariable String idEstudante){
        return ResponseEntity.ok(estudanteService.listarDisciplinas(idEstudante));
    }

}
