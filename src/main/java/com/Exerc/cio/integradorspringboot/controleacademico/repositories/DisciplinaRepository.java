package com.Exerc.cio.integradorspringboot.controleacademico.repositories;

import com.Exerc.cio.integradorspringboot.controleacademico.models.Disciplina;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DisciplinaRepository extends JpaRepository<Disciplina,Long> {

    Optional<Disciplina> getByCodigoAndTurma(String codigo, Integer turma);

    Optional<Disciplina> getByCodigoAndNomeAndTurma(String codigo, String nome, Integer turma);


}
