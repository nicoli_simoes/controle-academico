package com.Exerc.cio.integradorspringboot.controleacademico.models;

import com.Exerc.cio.integradorspringboot.controleacademico.listeners.EstudanteListener;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EntityListeners(EstudanteListener.class)
public class Estudante {

    @Id
    @Column(name = "numDocumento")
    @NotBlank(message = "Numero do documento é obrigatório!")
    private String numDocumento;

    @Column(name = "nome")
    @NotBlank(message = "Nome é obrigatório")
    private String  nome;


    private String matricula;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "endereco_id", referencedColumnName = "id")
    private Endereco endereco;

    @ManyToMany
    @JoinTable(
            name = "disciplinas_estudantes",
            joinColumns = @JoinColumn(name = "num_documento_estudante"),
            inverseJoinColumns = @JoinColumn(name = "id_disciplina")
    )
    @JsonIgnoreProperties("estudantes")
    private List<Disciplina> disciplinas;


    @CreationTimestamp
    private LocalDateTime dataDeCriacao;


}
