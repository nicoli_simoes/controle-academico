package com.Exerc.cio.integradorspringboot.controleacademico;


import com.Exerc.cio.integradorspringboot.controleacademico.exceptions.DisciplinaNaoEncontradaException;
import com.Exerc.cio.integradorspringboot.controleacademico.exceptions.EstudanteNaoEncontradoException;
import com.Exerc.cio.integradorspringboot.controleacademico.exceptions.IdInvalidoException;
import com.Exerc.cio.integradorspringboot.controleacademico.exceptions.MatriculaJaRealizadaException;
import com.Exerc.cio.integradorspringboot.controleacademico.listeners.EstudanteListener;
import com.Exerc.cio.integradorspringboot.controleacademico.models.Disciplina;
import com.Exerc.cio.integradorspringboot.controleacademico.models.Endereco;
import com.Exerc.cio.integradorspringboot.controleacademico.models.Estudante;
import com.Exerc.cio.integradorspringboot.controleacademico.repositories.DisciplinaRepository;
import com.Exerc.cio.integradorspringboot.controleacademico.repositories.EstudanteRepository;
import com.Exerc.cio.integradorspringboot.controleacademico.services.EstudanteService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.*;

public class EstudanteServiceTest extends ControleAcademicoApplicationTests{

    @MockBean
    private EstudanteRepository estudanteRepository;

    @MockBean
    private  DisciplinaRepository disciplinaRepository;

    @Autowired
    private EstudanteService estudanteService;

    private Estudante estudante= Estudante.builder()
            .nome("kayque")
            .numDocumento("123")
            .build();


    @Test
    public void deveAdicionarEstudante(){

        Mockito.when(estudanteRepository.save(estudante))
                .thenReturn(estudante);

        Assertions.assertEquals(estudante,estudanteService.adiciona(estudante));

        Mockito.verify(estudanteRepository).save(estudante);
    }

    @Test
    public void deveRetornarEstudantePeloNome(){
        Mockito.when(estudanteRepository.busca("%ay%"))
                .thenReturn(Arrays.asList(estudante));

        Assertions.assertEquals(Arrays.asList(estudante),estudanteService.buscaEstudantesPorNome("ay"));

        Mockito.verify(estudanteRepository,Mockito.times(2))
                .busca("%ay%");
    }

    @Test
    public void deveRetornarEstudantePelaMatricula(){

        Mockito.when(estudanteRepository.getByMatricula("123"))
                .thenReturn(Optional.ofNullable(estudante));

        Assertions.assertEquals(estudante,estudanteService.buscaPelaMatricula("123"));

        Mockito.verify(estudanteRepository).getByMatricula("123");
    }

    @Test
    public void  deveRetornarTodosEstudantes(){
        Pageable pageable = PageRequest.of(0, 5);

        Page<Estudante> page = new PageImpl<Estudante>(Arrays.asList(estudante));

        Mockito.when(estudanteRepository.findAll(pageable))
                .thenReturn(page);

        Assertions.assertEquals(page,estudanteService.listarTodos(pageable));

        Mockito.verify(estudanteRepository).findAll(pageable);
    }

    @Test
    public void deveAtualizarEstudante(){

        var estudanteRequisicao= Estudante.builder()
                .nome("abc")
                .build();

        var estudanteEsperado= Estudante.builder()
                .nome("abc")
                .numDocumento("123")
                .build();

        Mockito.when(estudanteRepository.findById("123"))
                .thenReturn(Optional.ofNullable(estudante));

        Mockito.when(estudanteRepository.save(estudanteEsperado))
                .thenReturn(estudanteEsperado);

        Assertions.assertEquals(estudanteEsperado,estudanteService.atualizar("123",estudanteRequisicao));


        Mockito.verify(estudanteRepository).findById("123");
        Mockito.verify(estudanteRepository).save(estudanteEsperado);

    }

    @Test
    public void deveLancarEstudanteNaoEncontradoExcepionNaBuscaPelaMatricula(){

        Mockito.when(estudanteRepository.getByMatricula("teste"))
                .thenReturn(Optional.ofNullable(estudante));

        Assertions.assertThrows(EstudanteNaoEncontradoException.class,()->estudanteService.buscaPelaMatricula("teste2"));

        Mockito.verify(estudanteRepository).getByMatricula("teste2");

    }

    @Test
    public void deveLancarEstudanteNaoEncontradoExcepionNaBuscaPorNome(){

        Mockito.when(estudanteRepository.busca(ArgumentMatchers.anyString()))
                .thenReturn(Collections.EMPTY_LIST);


        Assertions.assertThrows(EstudanteNaoEncontradoException.class,()->estudanteService.buscaEstudantesPorNome("teste2"));

        Mockito.verify(estudanteRepository).busca("%teste2%");

    }


    @Test
    public void deveLancarIdInvalidoExceptionAoAdicionarComIdJaCadastrado(){

        Mockito.when(estudanteRepository.existsById("123"))
                        .thenReturn(true);

        Assertions.assertThrows(IdInvalidoException.class,()->estudanteService.adiciona(estudante));

        Mockito.verify(estudanteRepository).existsById("123");

    }

    @Test
    public void deveAdicionarDisciplinaAoEstudante(){

        var disciplina= Disciplina.builder()
                .nome("calculo")
                .codigo("1")
                .turma(1)
                .build();

        var estudanteComDisciplina=Estudante.builder()
                .nome("kayque")
                .numDocumento("123")
                .disciplinas(Arrays.asList(disciplina))
                .build();

        Mockito.when(estudanteRepository.getByMatricula("777"))
                .thenReturn(Optional.of(estudante));


        Mockito.when(disciplinaRepository.getByCodigoAndTurma("1",1))
                        .thenReturn(Optional.of(disciplina));

        Mockito.when(estudanteRepository.save(estudante))
                        .thenReturn(estudanteComDisciplina);

        Assertions.assertEquals(estudanteComDisciplina,estudanteService.adicionarDisciplina("777","1",1));


        Mockito.verify(estudanteRepository).getByMatricula("777");
        Mockito.verify(disciplinaRepository).getByCodigoAndTurma("1",1);
        Mockito.verify(estudanteRepository).save(estudante);
    }

    @Test
    public void deveLancarrMatriculaJaRealizadaException(){

        var disciplina= Disciplina.builder()
                .nome("calculo")
                .codigo("1")
                .turma(1)
                .build();

        var estudanteComDisciplina=Estudante.builder()
                .nome("kayque")
                .numDocumento("123")
                .disciplinas(Arrays.asList(disciplina))
                .build();

        Mockito.when(estudanteRepository.getByMatricula("777"))
                .thenReturn(Optional.of(estudanteComDisciplina));


        Mockito.when(disciplinaRepository.getByCodigoAndTurma("1",1))
                .thenReturn(Optional.of(disciplina));

        Assertions.assertThrows(MatriculaJaRealizadaException.class,()->estudanteService.adicionarDisciplina("777","1",1));

    }

    @Test
    public void deveListarTodasDisciplinas(){

        var disciplina= Disciplina.builder()
                .nome("calculo")
                .codigo("1")
                .turma(1)
                .build();

        var estudanteComDisciplina=Estudante.builder()
                .nome("kayque")
                .numDocumento("123")
                .disciplinas(Arrays.asList(disciplina))
                .build();

        Mockito.when(estudanteRepository.findById("123"))
                .thenReturn(Optional.of(estudanteComDisciplina));

        Assertions.assertEquals(Arrays.asList(disciplina),estudanteService.listarDisciplinas("123"));

        Mockito.verify(estudanteRepository).findById("123");
    }


    @Test
    public void deveAdicionarEndereco(){
        var endereco= Endereco.builder()
                .cep("1234")
                .cidade("sbc")
                .estado("sp")
                .rua("rua tal")
                .build();

        var estudanteComEndereco=Estudante.builder()
                .nome("kayque")
                .numDocumento("123")
                .endereco(endereco)
                .build();


        Mockito.when(estudanteRepository.findById("123"))
                .thenReturn(Optional.of(estudante));

        Mockito.when(estudanteRepository.save(estudante))
                .thenReturn(estudanteComEndereco);

        Assertions.assertEquals(estudanteComEndereco,estudanteService.adicionaEndereco("123",endereco));

        Mockito.verify(estudanteRepository).findById("123");
        Mockito.verify(estudanteRepository).save(estudante);

    }

    @Test
    public void deveLancarDisciplinaNaoEncontradaException(){

        Mockito.when(estudanteRepository.findById("123"))
                .thenReturn(Optional.of(estudante));

        Assertions.assertThrows(DisciplinaNaoEncontradaException.class,()->estudanteService.listarDisciplinas("123"));

        Mockito.verify(estudanteRepository).findById("123");
    }

    @Test
    public  void deveGerarMatriculaAoCriar(){

       EstudanteListener spy= Mockito.spy(EstudanteListener.class);

       spy.antesDeCriar(estudante);

       Assertions.assertInstanceOf(String.class,estudante.getMatricula());
    }

}
