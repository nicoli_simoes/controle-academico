package com.Exerc.cio.integradorspringboot.controleacademico;

import com.Exerc.cio.integradorspringboot.controleacademico.controllers.DisciplinaController;
import com.Exerc.cio.integradorspringboot.controleacademico.models.Disciplina;
import com.Exerc.cio.integradorspringboot.controleacademico.services.DisciplinaService;
import com.google.gson.Gson;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.mockito.Mockito;

import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;



import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


@WebMvcTest(controllers= DisciplinaController.class)
public class DisciplinaControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DisciplinaService disciplinaService;

    private List<Disciplina> disciplinas=
            Arrays.asList(
                Disciplina.builder()
                    .codigo("mslamasl")
                    .nome("calculo I")
                    .horario('A')
                        .turma(1)
                    .build(),
                Disciplina.builder()
                    .codigo("INF1847")
                    .nome("Maeven")
                    .horario('C')
                    .build()
                );

    @Test
    public void deveAddDisciplinas() throws Exception {

        Mockito.when(disciplinaService.addDisciplina(disciplinas.get(0))).thenReturn(disciplinas.get(0));

        var json = new Gson().toJson(disciplinas.get(0));

        mockMvc.perform(post("/disciplinas/adicionar-disciplina").contentType("application/json").content(json))
                        .andExpect(MockMvcResultMatchers.status().isCreated())
                        .andExpect(jsonPath("$.codigo").value("mslamasl"))
                        .andExpect(jsonPath("$.nome").value("calculo I"))
                        .andExpect(jsonPath("$.horario").value("A"));

        Mockito.verify(disciplinaService).addDisciplina(disciplinas.get(0));
    }

    @Test
    public void deveRetornarTodasAsDisciplinas() throws Exception{


        Mockito.when(disciplinaService.listarTodas()).thenReturn(disciplinas);

        var json = new Gson().toJson(disciplinas);

        mockMvc.perform(get("/disciplinas").contentType("application/json").content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.[0].codigo").value("mslamasl"))
                .andExpect(jsonPath("$.[0].nome").value("calculo I"))
                .andExpect(jsonPath("$.[0].horario").value("A"))
                .andExpect(jsonPath("$.[1].codigo").value("INF1847"))
                .andExpect(jsonPath("$.[1].nome").value("Maeven"))
                .andExpect(jsonPath("$.[1].horario").value("C"));
        Mockito.verify(disciplinaService).listarTodas();
    }
}
