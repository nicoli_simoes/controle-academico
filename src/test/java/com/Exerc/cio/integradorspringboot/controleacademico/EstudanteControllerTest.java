package com.Exerc.cio.integradorspringboot.controleacademico;


import com.Exerc.cio.integradorspringboot.controleacademico.controllers.EstudanteController;
import com.Exerc.cio.integradorspringboot.controleacademico.exceptions.IdInvalidoException;
import com.Exerc.cio.integradorspringboot.controleacademico.models.Disciplina;
import com.Exerc.cio.integradorspringboot.controleacademico.models.Endereco;
import com.Exerc.cio.integradorspringboot.controleacademico.models.Estudante;
import com.Exerc.cio.integradorspringboot.controleacademico.services.EstudanteService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.web.servlet.MockMvc;
import com.google.gson.Gson;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(controllers = EstudanteController.class)
public class EstudanteControllerTest {

    @Autowired
    private  MockMvc mockMvc;

    @MockBean
    private EstudanteService estudanteService;

    private List<Estudante> estudantes=
            Arrays.asList(
                    Estudante.builder()
                            .nome("teste")
                            .numDocumento("teste")
                            .build(),
                    Estudante.builder()
                            .nome("teste")
                            .numDocumento("teste2")
                            .matricula("123")
                            .build()
            );


    @Test
    public void deveAdicionarEstudante() throws Exception {

        Mockito.when(estudanteService.adiciona(estudantes.get(0))).thenReturn(estudantes.get(0));

        var json = new Gson().toJson(estudantes.get(0));

        mockMvc.perform(post("/estudantes/adiciona")
                .contentType("application/json")
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(jsonPath("$.numDocumento").value("teste"))
                .andExpect(jsonPath("$.nome").value("teste"));

        Mockito.verify(estudanteService).adiciona(estudantes.get(0));
    }

    @Test
    public void deveRetornarEstudantePorNome() throws Exception {

        Mockito.when(estudanteService.buscaEstudantesPorNome("te")).thenReturn(estudantes);

        mockMvc.perform(post("/estudantes/buscaPorNome")
                        .param("termo","te"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("[0].numDocumento").value("teste"))
                .andExpect(jsonPath("[0].nome").value("teste"))
                .andExpect(jsonPath("[1].numDocumento").value("teste2"))
                .andExpect(jsonPath("[1].nome").value("teste"))
                .andExpect(jsonPath("[1].matricula").value("123"));

        Mockito.verify(estudanteService).buscaEstudantesPorNome("te");
    }


    @Test
    public void deveRetornarEstudantePelaMatricula() throws Exception {

        Mockito.when(estudanteService.buscaPelaMatricula("123")).thenReturn(estudantes.get(1));

        mockMvc.perform(get("/estudantes/buscaPorMatricula/{nMatricula}","123"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.numDocumento").value("teste2"))
                .andExpect(jsonPath("$.nome").value("teste"))
                .andExpect(jsonPath("$.matricula").value("123"));

        Mockito.verify(estudanteService).buscaPelaMatricula("123");
    }

    @Test
    public void deveRetornarTodosEstudantes() throws Exception {

        Pageable pageable = PageRequest.of(0, 5);

        Page<Estudante> page = new PageImpl<Estudante>(estudantes);

        Mockito.when(estudanteService.listarTodos(pageable)).thenReturn(page);
        mockMvc.perform(get("/estudantes"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.content[0].numDocumento").value("teste"))
                .andExpect(jsonPath("$.content[0].nome").value("teste"))
                .andExpect(jsonPath("$.content[1].numDocumento").value("teste2"))
                .andExpect(jsonPath("$.content[1].nome").value("teste"))
                .andExpect(jsonPath("$.content[1].matricula").value("123"));

        Mockito.verify(estudanteService).listarTodos(pageable);
    }

    @Test
    public void deveRetornarBadRequestAoCriarSemNumDocumento() throws Exception {

        var estudanteParaAdicionar=Estudante.builder()
                .nome("kayque")
                .build();


        var json= new Gson().toJson(estudanteParaAdicionar);

        mockMvc.perform(post("/estudantes/adiciona")
                .contentType("application/json")
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }

    @Test
    public void deveRetornarBadRequestAoCriarSemNome() throws Exception {

        var estudanteParaAdicionar = Estudante.builder()
                .numDocumento("123")
                .build();

        var json = new Gson().toJson(estudanteParaAdicionar);

        mockMvc.perform(post("/estudantes/adiciona")
                        .contentType("application/json")
                        .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void deveAtualizarEstudante() throws Exception {

        Mockito.when(estudanteService.atualizar("123",estudantes.get(0)))
                .thenReturn(estudantes.get(1));

        var json= new Gson().toJson(estudantes.get(0));

        mockMvc.perform(put("/estudantes/atualizar/{numDocumento}","123")
                .contentType("application/json")
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.numDocumento").value("teste2"))
                .andExpect(jsonPath("$.nome").value("teste"))
                .andExpect(jsonPath("$.matricula").value("123"));

        Mockito.verify(estudanteService).atualizar("123",estudantes.get(0));

    }


    @Test
    public void deveRetornarBadRequestAoCriarComDocumentoJaExistente() throws Exception {

        var json= new Gson().toJson(estudantes.get(0));

        Mockito.when(estudanteService.adiciona(estudantes.get(0)))
                        .thenThrow(new IdInvalidoException("Numero de documento ja cadastrado!"));

        mockMvc.perform(post("/estudantes/adiciona")
                .contentType("application/json")
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void deveAdicionarUmEndereco() throws Exception {

        var endereco= Endereco.builder()
                .cep("1234")
                .cidade("sbc")
                .estado("sp")
                .rua("rua tal")
                .build();

       Estudante estudante= estudantes.get(0);
       estudante.setEndereco(endereco);

        var json = new Gson().toJson(endereco);

        Mockito.when(estudanteService.adicionaEndereco("123",endereco))
                        .thenReturn(estudante);

        mockMvc.perform(post("/estudantes/{idEstudante}/adicionar/endereco","123")
                        .contentType("application/json")
                        .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.nome").value("teste"))
                .andExpect(jsonPath("$.numDocumento").value("teste"))
                .andExpect(jsonPath("$.endereco.cep").value("1234"))
                .andExpect(jsonPath("$.endereco.cidade").value("sbc"))
                .andExpect(jsonPath("$.endereco.estado").value("sp"))
                .andExpect(jsonPath("$.endereco.rua").value("rua tal"));

        Mockito.verify(estudanteService).adicionaEndereco("123",endereco);
    }

    @Test
    public void deveAdicionarDisciplina() throws Exception {
        var disciplina = Disciplina.builder()
                .nome("calculo")
                .codigo("1")
                .turma(1)
                .build();

        var estudante= Estudante.builder()
                .numDocumento("123")
                .nome("teste")
                .matricula("123")
                .disciplinas(Arrays.asList(disciplina))
                .build();


        Mockito.when(estudanteService.adicionarDisciplina("123","1",1))
                .thenReturn(estudante);

        mockMvc.perform(post("/estudantes/matricular")
                .param("matricula","123")
                .param("turma","1")
                .param("codigo","1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.nome").value("teste"))
                .andExpect(jsonPath("$.numDocumento").value("123"))
                .andExpect(jsonPath("$.disciplinas[0].nome").value("calculo"))
                .andExpect(jsonPath("$.disciplinas[0].codigo").value("1"))
                .andExpect(jsonPath("$.disciplinas[0].turma").value(1));

    }

    @Test
    public void deveRetornarDisciplinasDeUmAluno() throws Exception {
        var disciplina = Disciplina.builder()
                .nome("calculo")
                .codigo("1")
                .turma(1)
                .build();

        var estudante= Estudante.builder()
                .numDocumento("123")
                .nome("teste")
                .matricula("123")
                .disciplinas(Arrays.asList(disciplina))
                .build();

        Mockito.when(estudanteService.listarDisciplinas("123"))
                .thenReturn(estudante.getDisciplinas());

        mockMvc.perform(get("/estudantes/{idEstudante}/disciplinas","123"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.[0].nome").value("calculo"))
                .andExpect(jsonPath("$.[0].codigo").value("1"))
                .andExpect(jsonPath("$.[0].turma").value(1));
    }
}
